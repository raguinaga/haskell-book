-- An alternative way to write the sumSquare func with the let keyword
-- Whether to use let or where is largely a matter of style. They accomplish
-- the same thing
sumSquareOrSquareSum x y = let sumSquare = (x^2 + y^2)
                               squareSum = (x+y)^2
                           in --begin body of func
                             if sumSquare > squareSum
                             then sumSquare
                             else squareSum
