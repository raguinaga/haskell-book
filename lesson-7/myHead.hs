-- Use pattern matching to take the first element from a list
-- The second case deals with an empty list and just throws an error
myHead (x:xs) = x
myHead [] = error "No head for empty list"