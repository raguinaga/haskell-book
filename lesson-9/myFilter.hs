-- Creating the filter function assumming it doesn't exist
myFilter test [] = [] -- return an empty list if given an empty list

{- Else we test the first element using pattern matching -}
myFilter test (x:xs) = if test x
                       then x:myFilter test xs -- if it passes, cons the element to the rest of the filtered list
                       else myFilter test xs -- else don't cons it and move on to the other elements