{-
    Filter is another higher-order function that takes a list and function
    However, the function passed to filter must return true or false
-}

evenNumbers = filter even [1,2,3,4]
-- [2,4]

-- The lambda we supply returns true only if the first char in a string is "a"
fruits = filter (\(x:xs) -> x == 'a') ["apple", "banana", "avacado"]