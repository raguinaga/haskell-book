{-
    The final of the big 3 functions for using on lists is fold
    Fold takes a list and reduces it to a single value.
    There are two versions of fold: foldl and foldr (for left and right)
    fold in general takes 3 arguments: a binary operator, a init value and a list
-}

-- The most common scenario is to sum a list
sumOfFold = foldl (+) 0 [1,2,3,4]

-- Using partial app to build a concat function
concatAll = foldl (++) " " xs

-- You will frequently see map + fold used together
sumOfSquares xs = foldl (+) 0 (map (^2) xs)

-- You can also use fold to reverse a list but you need a helper function
rcons x y = y:x -- helper function
myReverse xs = foldl rcons [] xs