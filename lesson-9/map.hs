{-
    Using map, Haskell programmers can avoid explicitly writing recursive functions.
    Map takes a list and a function and then applies that function to every element in the list
-}
reverse_list = map reverse ["dog", "cat", "moose"]
-- ["god", "tac", "esoom"]

head_list = map head ["dog", "cat", "moose"]
-- "dcm"

first_four = map (take 4) ["pumpkin", "pie", "peanut butter"]
-- ["pump", "pie", "pean"]
