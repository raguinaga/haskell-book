{-
Lists are the fundamental data type in haskell
Lists are inherenetly recursive, building and deconstructing lists
are fundamental tools for a Functional programmer

 When taking apart a list, the head, tail and end are the most
 important parts, the head is the first element, the tail is
 literally everything else after the head. The actual end is
 represented by []

Building lists is important too. We need the cons operator ":"
This operator can add one value to the head of the list. To add
two lists, we use the concat "++" operator

-}

-- Here is a shorthand way to generate a range
list1 = [1..10]

steppedList = [1,3..10] -- Adding the next step, generates odd numbers

list2 = [1,1.5..5] -- increments the list by .5

list3 = [1,0..-10] -- generates a decrementing list

longList = [1..] -- generates an unending list.
                 -- This list can be stored and passed around
                 -- thanks to lazy evaluation
                 
{- Common functions on lists -}
-- The !! operator takes a list and returns the element specified by and inedx
element = [1,2,3] !! 0 -- Will return 1
char = "puppies" !! 4 -- Will return 'i'

-- You can use prefix notation and partial application to make functions
paExample = (!!) "dog" -- Now call and pass a number to get elements
paExample2 = ("dog" !!) -- I find this more readable
paExample3 = (!! 2) -- Pass words to get a char pos 2

-- There is also the length function, which returns the length
listLength = length [1..20] 
wordLength = length "quicksand"

-- Reverse, reverses a list
cheese = reverse "cheese"
countDown = reverse [1..5]

-- elem function checks if a value is in a list, returns a bool
thirteen = elem 13 [1..15] -- will return true
isP = elem 'p' "cheese" -- returns false

-- take takes a number + list and returns the first n elements of the list
take5 = take 5 [2,4..100]  -- 2, 4, 6, 8, 10
take3 = take 3 "wonderful" -- won

-- drop removes the elements of a list
drop_example = drop 2 [1,2,3,4,5] -- [3,4,5]
drop_example1 = drop 5 "very awesome" -- "awesome"

-- zip combines two lists into tuple pairs
zippedList = zip [1,2,3] [2,4,6] -- [(1,2),(2,4),(3,6)]
zippedAnimal = zip "dog" "rabbit" -- [('d','r'),('o','a'),('g','b')]

{- cycle uses lazy evaluation to create an infinite list -}
ones n = take n (cycle [1])
example1 = ones 2 -- [1,1]
example2 = ones 4 -- [1,1,1,1]

{- Here is another example using cycle. The scenario is to assign members
to groups across different domains -}
assignToGroups n aList = let groups = cycle [1..n]
                         in
                            zip groups aList