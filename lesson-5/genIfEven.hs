{- Here we write a function that applies another function -}
ifEven myFunction x = if even x
                      then myFunction x
                      else x

{- Here we write a function that returns functions. In this case, an entire
lambda is returned. Note that the lambda is waiting for x to be passed in .
Even more importantly, noteice that f is *captured* inside the lambda, this is
known as a closure-}
genIfEven f = (\x -> ifEven f x)

getRequestURL host apiKey resource id = host ++
                                        "/" ++
                                        resource ++
                                        "/" ++
                                        id ++
                                        "?token=" ++
                                        apiKey

{- We will continue using closures. In Haskell, when using closures, order
arguments from most to least general. Here we build a function that will return
a lambda that captures the host part of a url-}
genHostRequestBuilder host = (\apiKey resource id ->
                                getRequestUrl host apiKey resource id)

exampleURLBuilder = genHostRequestBuilder "http://example.com"
