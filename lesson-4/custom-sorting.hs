import Data.List -- Contains functions that work with lists
{-- 
    Here we introduce tuples that are like lists but can hold different types.
    Tuples are also of fixed size. Tuples of two items are so common, they 
    have a special terminology (a "pair"). Pairs also have special functions
    fst and snd, which spit out the first and second items respectivley.
--}

-- Here we create a list of pairs we want to sort
names = [("Ian", "Curtis"),
         ("Bernard", "Sumner"),
         ("Peter", "Hook"),
         ("Stephen", "Morris")]

-- Create a function to pass to the sortBy function in Data.List
{-- 
    The call will look like:
    sortBy compareLastNames names
 --}
compareLastNames name1 name2 = let lastName1 = snd name1
                                   lastName2 = snd name2
                                in
                                    if lastName1 > lastName2
                                    then GT
                                    else if lastName1 < lastName2
                                        then LT
                                        else EQ