{--
    This is a standard function that takes a number and increments it if n is
    even
--}
ifEvenInc n = if even n
              then n + 1
              else n

{-- 
    We can make this a generic function by using first-class functions.
    Here, we pass a number x to a func, but we also pass a function as an arg.
    If n is even, then we call our passed-in function and pass it x.
--}

ifEven myFunction x = if even x
                      then myFunction x
                      else x

-- Let's create some functions to use with the new ifEven
inc n = n + 1
double n = n*2
square n = n^2
