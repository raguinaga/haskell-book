-- Base case
myLength [] = 0

-- Alternate case
myLength (x:xs) = 1 + myLength xs