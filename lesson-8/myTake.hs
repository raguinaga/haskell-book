{-
 Take is interesting because of its two goal states
 Either it has an empty list, or the first arg is 0
 Both return an empty list
-}
myTake _ [] = [] -- return an empty list if handed an empty list
myTake 0 _ = []  -- return an empty list if we take zero
{- Else take n amounts of elements
We call take over and over as we use n-1 to iterate over an ever shortening list
take itself uses x to build a new list out of the taken elements -}
myTake n (x:xs) = let rest = myTake (n-1) xs
                  in
                    x:rest